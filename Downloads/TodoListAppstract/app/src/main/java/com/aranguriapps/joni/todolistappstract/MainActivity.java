package com.aranguriapps.joni.todolistappstract;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aranguriapps.joni.todolistappstract.Fragments.ItemFragment;
import com.aranguriapps.joni.todolistappstract.Fragments.Opciones;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,GoogleApiClient.OnConnectionFailedListener,Opciones.OnFragmentInteractionListener,ItemFragment.OnListFragmentInteractionListener {


    private GoogleApiClient googleApiClient;
    private FloatingActionButton fab;
    private  NavigationView navigationView;
    private View hView;
    private ImageView deleteImage;
    private DatabaseReference database;
    private DatabaseReference referenciaUsuario;
    private SharedPreferences sharedPref;
    private long idLasItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleSignInOptions gso= new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
        //pongo un fragment default
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_base,new ItemFragment()).commit();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //obtengo la referencia para el menu lateral
         navigationView = (NavigationView) findViewById(R.id.nav_view);
         hView =  navigationView.getHeaderView(0);
        //referencia al boton de cerrar sesion
        ImageView cerrarSesionImg=(ImageView)hView.findViewById(R.id.apagar);
        cerrarSesionImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });
        //referencia a las preferencias que sirven para asignarle un id a las tareas/items
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        //referencia al boton de agregar
         fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            agregarItem(view);}
        });
        //referencia a la Base de datos
         database = FirebaseDatabase.getInstance().getReference();
         referenciaUsuario = database.child("Usuarios").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Items");
        //listener para el menu lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void salir() {

            new AlertDialog.Builder(this)
                    .setTitle("Cerrar Sesión")
                    .setMessage("Seguro que desea cerrar la sesión?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            cerrarSesion();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // No hacer nada
                        }
                    })
                    .show();

    }

    private void agregarItem(View view) {
        //aca muestro un alert donde el ususrio ingresa la tarea que quiere agregar
        final EditText taskEditText = new EditText(view.getContext());
        AlertDialog dialog = new AlertDialog.Builder(view.getContext())
                .setTitle("Agregar una nueva tarea")
                .setMessage("Qué es lo que querés recordar?")
                .setView(taskEditText)
                .setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        idLasItem = sharedPref.getLong(FirebaseAuth.getInstance().getCurrentUser().getUid(), 0);
                        ItemLista nuevoitem = new ItemLista();
                        nuevoitem.setTexto(String.valueOf(taskEditText.getText()));
                        nuevoitem.setId(idLasItem+1+"");
                        referenciaUsuario.child(idLasItem+1+"").setValue(nuevoitem);

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putLong(FirebaseAuth.getInstance().getCurrentUser().getUid(), idLasItem+1);
                        editor.commit();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //no hacer nada


                    }
                })
                .create();
        dialog.show();


    }

    @Override
    public void onBackPressed() {
        //es para que cerrar el menu al presionar el boton de atras
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //aca se selecciona lo que se hace al presionar alguna de las opciones del menu lateral
        int id = item.getItemId();
Fragment fragment= null;
if (id == R.id.nav_lista) {
            fab.setVisibility(View.VISIBLE);
            fragment= new ItemFragment();
            // Handle the camera action
        } else if (id == R.id.nav_mail) {
            mandarMail();
            return true;

        } else if (id == R.id.nav_opciones) {
            fab.setVisibility(View.INVISIBLE);
            fragment= new Opciones();

        } else if (id == R.id.nav_sobre_mi) {
            verMiPerfil();
            return true;

        } else if (id == R.id.nav_share) {
            compartirApp();
            return  true;

        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_base,fragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void mandarMail() {
        //aca se manda el mail para contratarme
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto: arangurim@gmail.com"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT,"Confirmación APPSTRACT");
        emailIntent.putExtra(Intent.EXTRA_TEXT,"Joni, estás contratado..");
        startActivity(Intent.createChooser(emailIntent, "Enviar correo"));

    }

    private void verMiPerfil() {
        //aca ves mi perfil de linkedIn
        Uri uri2 = Uri.parse("https://www.linkedin.com/in/jonathan-aranguri"); // missing 'http://' will cause crashed
        Intent intent2 = new Intent(Intent.ACTION_VIEW, uri2);
        startActivity(intent2);
        overridePendingTransition(R.anim.fade_in,0);

    }

    private void compartirApp() {
        try {
            //Acá iría el link de esta app una vez subida a Google Play pero no llegué a subirla asi que puse el de una linterna mía
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "TodoListAppstract");
            String sAux = "\nTe recomiendo esta app\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.aranguriapps.familiaaranguri.simplelinterna&hl=es \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Elegí una"));
        } catch(Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        //aca veo si me llegan los datos del usuario que inicio sesión
        OptionalPendingResult<GoogleSignInResult> opr=Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(opr.isDone()){
            GoogleSignInResult resultado=opr.get();
            manejarResultado(resultado);
        }else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    manejarResultado(googleSignInResult);
                }
            });
        }
    }

    private void manejarResultado(GoogleSignInResult resultado) {
        //aca si se pude loguear muestro los datos del usuario en el menu lateral
        if(resultado.isSuccess()){
            GoogleSignInAccount cuenta= resultado.getSignInAccount();
            TextView userName = (TextView)hView.findViewById(R.id.userName);
            userName.setText(cuenta.getDisplayName());
            TextView userMail=(TextView)hView.findViewById(R.id.userMail);
          userMail.setText(cuenta.getEmail());
           ImageView userImage= (ImageView)hView.findViewById(R.id.userImage);



           Glide.with(this).load(cuenta.getPhotoUrl()).into(userImage);

        }else Loguearse();

    }

    private void Loguearse() {
        //aca me voy a la activity de log_in
        Intent intent = new Intent(this,Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.go_away, R.anim.nada);    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    public void cerrarSesion(){
        //aca cierro la sesión y me voy a la pagina de log-in
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if(status.isSuccess())
                    Loguearse();
                else Toast.makeText(MainActivity.this, "No se pudo cerrar sesión", Toast.LENGTH_SHORT).show();
            }
        });
    }


     @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onListFragmentInteraction(ItemLista item) {

    }
}
