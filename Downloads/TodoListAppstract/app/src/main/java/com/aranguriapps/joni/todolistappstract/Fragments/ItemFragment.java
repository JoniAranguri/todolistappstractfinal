package com.aranguriapps.joni.todolistappstract.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aranguriapps.joni.todolistappstract.ItemLista;
import com.aranguriapps.joni.todolistappstract.R;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.ArrayList;


public class ItemFragment extends Fragment implements ChildEventListener {
//Este es el fragment "más importante" donde está la lista de Tareas/items que ingresa el usuario
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    private FirebaseDatabase database;
    private MyItemRecyclerViewAdapter adaptador;
    private DatabaseReference referenciaUsuario;



    public ItemFragment() {
    }
    public static ItemFragment newInstance(int columnCount) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = FirebaseDatabase.getInstance();
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
         referenciaUsuario = database.child("Usuarios").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Items");
        referenciaUsuario.addChildEventListener(this);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            ArrayList<ItemLista> arra=new ArrayList<ItemLista>();
           adaptador=new MyItemRecyclerViewAdapter(arra);
            //no usado, al final no implemente la opcion de eliminar porque no se pedia eso y me iba a tardar mas ...
            adaptador.setOnLongClickListener(new View.OnLongClickListener(){

                @Override
                public boolean onLongClick(View view) {
                    Toast.makeText(getContext(), view.toString()+"estoy en Long", Toast.LENGTH_SHORT).show();
                    view.setBackgroundColor(244);
                    return  true;
                }
            });
            recyclerView.setAdapter(adaptador);
        }
        return view;
    }
    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        //cuando se agrega un hijo al nodo de usuario , lo agrego a la lista tambien
        ItemLista si=dataSnapshot.getValue(ItemLista.class);
        adaptador.agregarTarea(si);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
  //nada

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

        //si se elimina de la base de datos lo elimino de la lista tambien
        ItemLista si=dataSnapshot.getValue(ItemLista.class);
        adaptador.removerTarea(si);




    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//nada
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        //nada
    }
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(ItemLista item);
    }


}
