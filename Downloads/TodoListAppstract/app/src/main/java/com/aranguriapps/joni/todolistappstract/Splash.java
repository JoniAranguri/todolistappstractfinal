package com.aranguriapps.joni.todolistappstract;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class Splash extends AppCompatActivity {
    protected Animation fadeIn;
    protected ImageView iconoappstract, iconoaranguri,iconoapp;
    TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        iconoappstract=(ImageView)findViewById(R.id.iconoappstract) ;
        iconoaranguri=(ImageView)findViewById(R.id.iconoaranguri);
        textView2=(TextView)findViewById(R.id.textView2);
        iconoapp=(ImageView)findViewById(R.id.iconoapp) ;
        fadeIn = AnimationUtils.loadAnimation(this,R.anim.fade_in);

//Ccrro tres threads para mostrar un splash con animacion y tres imagenes
        Thread timerThread = new Thread(){
            public void run() {



                iconoappstract.startAnimation(fadeIn);
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                iconoappstract.setImageBitmap(null);
                                iconoaranguri.startAnimation(fadeIn);
                            }
                        }, 1000);


                    }
                });
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                iconoaranguri.setImageBitmap(null);
                                iconoapp.startAnimation(fadeIn);
                                textView2.setVisibility(View.VISIBLE);//CAMBIAR ESTOOOOOO
                            }
                        }, 4000);


                    }
                });
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                               cerrarSplash();
                            }
                        }, 7000);


                    }
                });

            }
        };
        timerThread.start();
    }

    private void cerrarSplash() {
        //me voy a al login

            Intent intent = new Intent(Splash.this,Login.class);
            startActivity(intent);
        overridePendingTransition(R.anim.slide_left, R.anim.nada);

    }

    @Override
    protected void onPause() {
        // para que no siga activa la activity
        super.onPause();
        finish();
    }


}
