package com.aranguriapps.joni.todolistappstract.Fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.aranguriapps.joni.todolistappstract.ItemLista;
import com.aranguriapps.joni.todolistappstract.R;
import java.util.ArrayList;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> implements View.OnLongClickListener {
//Este es el adaptador para el la lista de Tareas/items
    private ArrayList<ItemLista> mValues;
    private View.OnLongClickListener listener;


    public MyItemRecyclerViewAdapter(ArrayList<ItemLista> lista) {
        mValues = lista;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        view.setOnLongClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mIdView.setText("* "+mValues.get(position).getTexto());


    }

    public void removerTarea(ItemLista item) {
        //metodo no usado
        int posicion = mValues.indexOf(item);
        mValues.remove(item);
        notifyItemRemoved(posicion);


    }

    public void agregarTarea(ItemLista item) {
        //agrega a la lista del fragment, los items/tareas que se van cargando desde la base de datos

        if (!mValues.contains(item)) {
            mValues.add(item);
            notifyItemInserted(mValues.size() - 1);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
    public void setOnLongClickListener(View.OnLongClickListener listener){
        this.listener= listener;
    }

    @Override
    public boolean onLongClick(View view) {
        //no usado, lo iba a usar para eliminar items de la lista pero no era lo que se pedia
        if(listener!=null)listener.onLongClick(view);
        return  true;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.miItem);
        }
        }


}
