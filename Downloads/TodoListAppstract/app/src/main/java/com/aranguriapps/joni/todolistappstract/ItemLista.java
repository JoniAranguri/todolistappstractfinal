package com.aranguriapps.joni.todolistappstract;

import android.content.ClipData;

/**
 * Created by arang on 3/12/2017.
 */

public class ItemLista {
    //Esta es la clase que en rigor debería llamarse Tarea pero bueno es lo mismo, tiena id y texto nada mas.
private String textoItem;
   public String id;
    public void setTexto(String textoItem){
        this.textoItem=textoItem;
    }
  public String  getTexto(){return textoItem;}
    public void setId(String id){
        this.id=id;
    }
    public String getId(){return id;}
    @Override
    public boolean equals(Object o) {

        ItemLista item= (ItemLista)o;

        if(this.id.equals(item.getId())&&this.textoItem.equals(item.getTexto())){
            return true;
        }
        return  false;
    }
}
