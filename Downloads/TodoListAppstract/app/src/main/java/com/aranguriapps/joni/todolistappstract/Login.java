package com.aranguriapps.joni.todolistappstract;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
private GoogleApiClient googleApiClient;
    private SignInButton sign_in_button;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //referencia a la barra de progreso de log:in
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //seteo lo que pide google para la auth
        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
        //referencia al boton de sign_in
        sign_in_button=(SignInButton)findViewById(R.id.sign_in_button);
        sign_in_button.setVisibility(View.VISIBLE);
        sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!hayInternet()){
                    Snackbar.make(view, "No hay conecci�n a internet", Snackbar.LENGTH_LONG)
                            .show();
                }else {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,777);}


            }
        });

        firebaseAuth= FirebaseAuth.getInstance();
        firebaseAuthListener= new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser usuario= firebaseAuth.getCurrentUser();
               // if(usuario!=null)irAlMenu();

            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(firebaseAuthListener!=null)
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == 777) {
                progressBar.setVisibility(View.VISIBLE);
                sign_in_button.setVisibility(View.GONE);
                GoogleSignInResult resultado = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                manejarResultado(resultado);
            }
        }


    private void manejarResultado(GoogleSignInResult resultado) {
        if(resultado.isSuccess()) firebaseAutenticacion(resultado.getSignInAccount());
        else Toast.makeText(this, "No se pudo iniciar sesi�n", Toast.LENGTH_SHORT).show();

    }

    private void firebaseAutenticacion(GoogleSignInAccount signInAccount) {
        AuthCredential credencial = GoogleAuthProvider.getCredential(signInAccount.getIdToken(),null);
        firebaseAuth.signInWithCredential(credencial).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful())
                    Toast.makeText(Login.this, "No se pudo iniciar sesi�n en FireBase", Toast.LENGTH_SHORT).show();
                else irAlMenu();
            }
        });
    }

    private void irAlMenu() {

        Intent intent = new Intent(this,MainActivity.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.slide_left, R.anim.nada);


    }
    private boolean hayInternet() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // veo si hay coneccion
        return (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) ;

    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
